import {Component, OnInit} from '@angular/core';
import {User} from "./user";
import {ApiService} from "../_services/api.service";
import {StorageService} from "../_services/storage.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {

  users: User[] = [];
  isLoading: boolean = false;
  loggedInUser: string = "";

  constructor(private apiService: ApiService, private storageService: StorageService, private router: Router) {
  }

  ngOnInit(): void {
    this.loggedInUser = this.storageService.user;
    this.fetchUsers();
  }

  fetchUsers() {
    this.isLoading = true;
    this.apiService.getUsers().subscribe(data => {
      this.users = [];
      Object.entries(data).forEach((key: [any, any]) => this.users.push({
        id: key[1].id,
        username: key[1].username,
        enabled: key[1].enabled
      }));
      this.users.sort((a, b) => (a.id > b.id ? 1 : -1));
    });
    this.isLoading = false;
  }

  updateUser(user: User) {
    this.apiService.updateUser(user.id, !user.enabled)
      .subscribe({
        next: () => {
          this.fetchUsers();
        }
      });
  }

  deleteUser(id: number) {
    this.apiService.deleteUser(id)
      .subscribe({
        next: () => {
          this.fetchUsers();
        }
      });
  }

  addUser() {
    this.router.navigate(['/users/add']);
  }

}
