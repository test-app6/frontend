import {Component} from '@angular/core';
import {ApiService} from "../../_services/api.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent {

  form: any = {
    username: null,
    password: null
  };
  requestFailed = false;
  errorMessage = '';

  constructor(private apiService: ApiService, private router: Router) {
  }

  onSubmit(): void {
    const {username, password} = this.form;

    this.apiService.addUser(username, password)
      .subscribe({
        next: () => {
          this.requestFailed = false;
          this.navigateBack();
        },
        error: err => {
          if (err.status == 400) {
            this.errorMessage = err.error;
          } else {
            this.errorMessage = 'Something went wrong. Try again later.';
          }
          this.requestFailed = true;
        }
      });
  }

  navigateBack() {
    this.router.navigate(['/users']);
  }
}
