import {Component, OnInit} from '@angular/core';
import {StorageService} from '../_services/storage.service';
import {ApiService} from "../_services/api.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {
    username: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';

  constructor(private apiService: ApiService, private storageService: StorageService, private router: Router) {
  }

  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
    }
  }

  onSubmit(): void {
    const {username, password} = this.form;

    this.apiService.login(username, password)
      .subscribe({
        next: data => {
          let token = data.headers.get("Authorization")?.toString()!;
          this.storageService.saveUser(username, token);

          this.isLoginFailed = false;
          this.isLoggedIn = true;
          this.router.navigate(['/users']).then(() =>
            window.location.reload()
          );
        },
        error: err => {
          if (err.status == 401) {
            this.errorMessage = 'Invalid credentials';
          } else {
            this.errorMessage = 'Something went wrong. Try again later.';
          }
          this.isLoginFailed = true;
        }
      });
  }
}
