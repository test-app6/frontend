import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private readonly TOKEN = 'jwt_token';
  private readonly USER = 'auth_user';

  constructor() {
  }

  clean(): void {
    localStorage.clear();
  }

  get token(): any {
    return localStorage.getItem(this.TOKEN);
  }

  get user(): any {
    return localStorage.getItem(this.USER);
  }

  public saveUser(user: string, token: string): void {
    localStorage.removeItem(this.USER);
    localStorage.removeItem(this.TOKEN);
    localStorage.setItem(this.USER, user);
    localStorage.setItem(this.TOKEN, token);
  }

  public isLoggedIn(): boolean {
    const user = localStorage.getItem(this.USER);
    return !!user;

  }
}
