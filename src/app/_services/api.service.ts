import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) {
  }

  login(username: string, password: string) {
    return this.http.post(`${environment.baseUrl}/login`, {
      username,
      password
    }, {headers: new HttpHeaders({'Content-Type': 'application/json'}), observe: "response"})
  }

  getUsers() {
    return this.http.get(`${environment.baseUrl}/users`, httpOptions);

  }

  addUser(username: string, password: string) {
    return this.http.post(`${environment.baseUrl}/users`, {
      username,
      password
    }, httpOptions);
  }

  updateUser(id: number, enabled: boolean) {
    // let queryParams = new HttpParams();
    // queryParams.append('enabled', enabled);
    return this.http.put(`${environment.baseUrl}/users/${id}?enabled=${enabled}`, httpOptions);
  }

  deleteUser(id: number) {
    return this.http.delete(`${environment.baseUrl}/users/${id}`, httpOptions);
  }
}
