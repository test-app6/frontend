import {Injectable} from '@angular/core';
import {BehaviorSubject, tap} from "rxjs";
import {ApiService} from "./api.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly TOKEN_NAME = 'jwt_token';
  private readonly USER = 'auth_user';
  isLoggedIn$ = new BehaviorSubject<boolean>(false);

  get token(): any {
    return localStorage.getItem(this.TOKEN_NAME);
  }

  get user(): any {
    return localStorage.getItem(this.USER);
  }

  constructor(private apiService: ApiService) {
    this.isLoggedIn$.next(!!this.token);
  }

  login(username: string, password: string) {
    return this.apiService.login(username, password).pipe(
      tap((response: any) => {
        console.log(response.headers.get);
        console.log(response.headers.get('Authorization'));
        // console.log(response.headers)
        response.headers.getItem("");
        this.isLoggedIn$.next(true);
        localStorage.setItem(this.USER, username);
        localStorage.setItem(this.TOKEN_NAME, response.token);
      })
    );
  }

  logout() {
    localStorage.removeItem(this.USER);
    localStorage.removeItem(this.TOKEN_NAME);
    this.isLoggedIn$.next(false);
  }
}
