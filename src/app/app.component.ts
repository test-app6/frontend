import {Component} from '@angular/core';
import {StorageService} from './_services/storage.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isLoggedIn = false;
  username?: string;

  constructor(private storageService: StorageService, private router: Router) {
  }

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      this.username = this.storageService.user;
    }
  }

  logout(): void {
    this.storageService.clean();
    this.router.navigate(['/login']).then(() => window.location.reload());
  }
}
